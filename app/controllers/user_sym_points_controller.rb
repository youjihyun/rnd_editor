class UserSymPointsController < ApplicationController
	before_action :set_user_sym_point, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:create, :destroy]

	def create
    @user_sym_point = UserSymPoint.new(user_sym_point_params)

    @user_sym_point.user_id = current_user.id

    @user_sym_point.save

    respond_to do |format|
      if @user_sym_point.save
        format.html { redirect_to restaurant_path(@user_sym_point.restaurant_id), notice: '공감하셨습니다.' }
        format.json { render :show, status: :created, location: @restaurant }
      else
        format.html { render :new }
        format.json { render json: @user_sym_point.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @user_sym_point.destroy
    respond_to do |format|
      format.html { redirect_to restaurant_path(@user_sym_point.restaurant_id), notice: '공감 취소' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_sym_point
      @user_sym_point = UserSymPoint.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_sym_point_params
      params.require(:user_sym_point).permit(:restaurant_id, :user_id)
    end
end
