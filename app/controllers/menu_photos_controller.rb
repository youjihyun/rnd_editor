class MenuPhotosController < ApplicationController
	before_action :set_restaurant
	before_action :set_menu
  before_action :set_menu_photo, only: [:show, :edit, :update, :destroy]

  # GET /menus
  # GET /menus.json
  def index
  end

  def home
  end

  # GET /menus/1
  # GET /menus/1.json
  def show
  end

  # GET /menus/new
  def new
  end

  # GET /menus/1/edit
  def edit
    
  end

  # POST /menus
  # POST /menus.json
  def create
   
  end

  # PATCH/PUT /menus/1
  # PATCH/PUT /menus/1.json
  def update
    respond_to do |format|
      if @menu_photo.update(menu_photo_params)
      	if params[:photo].blank?
      		format.html { render :action => "crop" }
	        # format.html { redirect_to [@menu.restaurant, @menu], notice: 'Menu was successfully updated.' }
	        # format.json { render :show, status: :ok, location: @menu }
	      else
	      	format.html { render :action => "crop" }
	      end
      else
        format.html { render :edit }
        format.json { render json: @menu.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /menus/1
  # DELETE /menus/1.json
  def destroy
    @menu_photo.destroy
    respond_to do |format|
      format.html { redirect_to [@menu.restaurant, @menu], notice: 'Menu was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_restaurant
      @restaurant = Restaurant.find(params[:restaurant_id])
    end

    def set_menu
      @menu = @restaurant.menus.find(params[:menu_id])
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_menu_photo
      @menu_photo = @menu.menu_photos.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def menu_photo_params
      params.require(:menu_photo).permit(:restaurant_id, :menu_id,:crop_x, :crop_y, :crop_w, :crop_h)
    end
end
