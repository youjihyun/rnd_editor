class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :set_locale

  def set_locale
    if !params[:locale].blank?
  	  I18n.locale = session[:user_language] = params[:locale]
  	else
  		I18n.locale  = session[:user_language] || I18n.default_locale
    end
	end
end
