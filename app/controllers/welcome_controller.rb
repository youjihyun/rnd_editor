class WelcomeController < ApplicationController
  def index
  	@restaurants = Restaurant.all
  end

  def login
  	@restaurant = Restaurant.find_by_r_code(params[:r_code])

  	if @restaurant.r_pwd == params[:r_pwd] then
  		respond_to do |format|
	      format.html { redirect_to restaurant_menus_path(@restaurant.id), notice: 'LogIn success.' }
	      format.json { head :no_content }
	    end
    end
  end

end
