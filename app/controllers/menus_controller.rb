class MenusController < ApplicationController
  before_action :set_restaurant
  before_action :set_menu, only: [:show, :edit, :update, :destroy]

  # GET /menus
  # GET /menus.json
  def home
  end

  def index
    @menus = @restaurant.menus.all
    @categories = @restaurant.categories.all
  end

  # GET /menus/1
  # GET /menus/1.json
  def show
    if @menu.category_id
      @category = @restaurant.categories.find(@menu.category_id)
    end
  end

  # GET /menus/new
  def new
    @menu = @restaurant.menus.new
    @categories = @restaurant.categories.all
  end

  # GET /menus/1/edit
  def edit
    @categories = @restaurant.categories.all
  end

  # POST /menus
  # POST /menus.json
  def create
    @menu = @restaurant.menus.new(menu_params)

    respond_to do |format|
      if @menu.save
        unless params[:menu_videos].blank?
          @menu.create_videos(params[:menu_videos])
        end
        unless params[:menu_translations].blank?
          @menu.create_translations(params[:menu_translations])
        end
        unless params[:menu_photos].blank?
          @menu.create_photos(params[:menu_photos])
          # format.html { render :action => "edit_photo" }
        end
        format.html { redirect_to [@menu.restaurant, @menu], notice: 'Menu was successfully created.' }
        format.json { render :show, status: :created, location: @menu }
      else
        format.html { render :new }
        format.json { render json: @menu.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /menus/1
  # PATCH/PUT /menus/1.json
  def update
    respond_to do |format|
      if @menu.update(menu_params)
        unless params[:menu_videos].blank?
          @menu.create_videos(params[:menu_videos])
        end
        unless params[:menu_translations].blank?
          @menu.create_translations(params[:menu_translations])
        end
        unless params[:menu_photos].blank?
          @menu.create_photos(params[:menu_photos])
          # format.html { render :action => "edit_photo" }
        end
        format.html { redirect_to [@menu.restaurant, @menu], notice: 'Menu was successfully updated.' }
        format.json { render :show, status: :ok, location: @menu }
      else
        format.html { render :edit }
        format.json { render json: @menu.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /menus/1
  # DELETE /menus/1.json
  def destroy
    @menu.destroy
    respond_to do |format|
      format.html { redirect_to [@menu.restaurant, @menu], notice: 'Menu was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_restaurant
      @restaurant = Restaurant.find(params[:restaurant_id])
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_menu
      @menu = @restaurant.menus.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def menu_params
      params.require(:menu).permit(:name, :price, :description, :category_id, :menu_translations, :price_usd, :price_jpy, :price_cny)
    end
end
