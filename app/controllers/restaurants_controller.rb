class RestaurantsController < ApplicationController
  before_action :set_restaurant, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:new]

  # GET /restaurants
  # GET /restaurants.json
  def index
    @restaurants = Restaurant.all
  end

  # GET /restaurants/1
  # GET /restaurants/1.json
  def show
    @comments = @restaurant.comments.all

    @sum_star_point = 0

    @comments.each do |comment|
      @sum_star_point = @sum_star_point + comment.star_point
    end
    if @comments.count == 0
      @average_star_point = 0
    else
      @average_star_point = @sum_star_point / @comments.count
    end

    if user_signed_in?
      @user_sym_points = UserSymPoint.where(:restaurant_id => @restaurant.id, :user_id => current_user.id)
      # find_all_by_user_id(current_user.id)
      if @user_sym_points.blank?
        @user_sym_point = UserSymPoint.new
      end
    end
      @count = UserSymPoint.where(:restaurant_id => @restaurant.id).count
  end

  # GET /restaurants/new
  def new
    @restaurant = Restaurant.new
  end

  # GET /restaurants/1/edit
  def edit
  end

  # POST /restaurants
  # POST /restaurants.json
  def create
    @restaurant = Restaurant.new(restaurant_params)

    @restaurant.user_id = current_user.id

    respond_to do |format|
      if @restaurant.save
        unless params[:restaurant_photos].blank?
          @restaurant.create_photos(params[:restaurant_photos])          
        end
        format.html { redirect_to @restaurant, notice: 'Restaurant was successfully created.' }
        format.json { render :show, status: :created, location: @restaurant }
      else
        format.html { render :new }
        format.json { render json: @restaurant.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /restaurants/1
  # PATCH/PUT /restaurants/1.json
  def update
    respond_to do |format|
      if @restaurant.update_attributes(restaurant_params)
        unless params[:restaurant_photos].blank?
          @restaurant.create_photos(params[:restaurant_photos])
        end
        format.html { redirect_to @restaurant, notice: 'Restaurant was successfully updated.' }
        format.json { render :show, status: :ok, location: @restaurant }
      else
        format.html { render :edit }
        format.json { render json: @restaurant.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /restaurants/1
  # DELETE /restaurants/1.json
  def destroy
    @restaurant.destroy
    respond_to do |format|
      format.html { redirect_to restaurants_url, notice: 'Restaurant was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_restaurant
      @restaurant = Restaurant.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def restaurant_params
      # params[:restaurant]
      params.require(:restaurant).permit(:name, :description, :r_code, :r_pwd, :open_time, :close_time, :mon_d, :tue_d, :wed_d, :thu_d, :fri_d, :sat_d, :sun_d, :postcode, :address, :x_pos, :y_pos, :phone)
    end
end
