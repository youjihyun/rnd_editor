class MenuPhoto < ActiveRecord::Base
	belongs_to :menu

	# rails 4에서는 없는 메소드
	# attr_accessible :menu_id, :photo, :seq
  
  # has_attached_file :photo, :styles => { 
  #   :large => "577x429#",
  #   :medium => "217x163#", 
  #   :small => "128x96#", 
  #   :thumb => "76x57#",
  #   :mini => "50x37#" }, :default_url => '/images/:style/missing.png', :processors => [:cropper]

  has_attached_file :photo, :styles => {
    :small => {:geometry => "300x200#", :processors => [:cropper]},
    :large => {:geometry => "600x400#", :processors => [:cropper]}
  }

  attr_accessor :crop_x, :crop_y, :crop_w, :crop_h
  after_update :reprocess_photo, :if => :cropping?

  def url(type)
    self.photo.url(type)
  end
  
  def cropping?
    !crop_x.blank? && !crop_y.blank? && !crop_w.blank? && !crop_h.blank?
  end
  
  def avatar_geometry(style = :original)
    @geometry ||= {}
    @geometry[style] ||= Paperclip::Geometry.from_file(photo.path(style))
  end
  
  private
  
  def reprocess_photo
    # photo.reprocess!
    photo.assign(photo)
    photo.save
  end

  validates_attachment_content_type :photo, :content_type => /\Aimage\/.*\Z/

  PHOTO_STYLE = {
    :large => "577x429",
    :medium => "217x163",  
    :small => "128x96", 
    :thumb => "76x57",
    :mini => "50x37"
  }

end