class RestaurantPhoto < ActiveRecord::Base
	belongs_to :restaurant

	has_attached_file :photo, :styles => { 
    :large => "577x429#",
    :medium => "217x163#", 
    :small => "128x96#", 
    :thumb => "76x57#",
    :mini => "50x37#" }, :default_url => '/images/:style/missing.png'

  validates_attachment_content_type :photo, :content_type => /\Aimage\/.*\Z/

  PHOTO_STYLE = {
    :large => "577x429#",
    :medium => "217x163#",  
    :small => "128x96#", 
    :thumb => "76x57#",
    :mini => "50x37#"
  }

  def url(type)
  	self.photo.url(type)
  end
end
