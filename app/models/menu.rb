class Menu < ActiveRecord::Base
	belongs_to :restaurant
	belongs_to :category

	has_many :menu_photos, dependent: :destroy
  has_many :menu_videos, dependent: :destroy
  has_many :menu_translations, dependent: :destroy

	def photos
  	self.menu_photos.order('seq')
  end

  def create_photos(photos)
    seq = self.photos.count

    photos.each do |p|
      seq = seq + 1
      self.menu_photos.create(:photo=>p, :seq=>seq)
    end
  end

  def main_photo(type)
    if self.menu_photos.order('seq').first
      self.menu_photos.order('seq').first.url(type)
    else
      "/assets/logo/restaurant#{RestaurantPhoto::PHOTO_STYLE[type]}.png"
    end
  end

  def videos
    self.menu_videos.order('seq')
  end

  def create_videos(videos)
    seq = self.videos.count

    videos.each do |v|
      seq = seq + 1
      self.menu_videos.create(:video=>v, :seq=>seq)
    end
  end

  def main_video(type)
    if self.menu_videos.order('seq').first
      self.menu_videos.order('seq').first.url(type)
    else
      "/assets/logo/restaurant#{RestaurantPhoto::PHOTO_STYLE[type]}.png"
    end
  end

  def translations
    self.menu_translations.order('locale')
  end

  def locale_translation(locale)
    self.menu_translations.find_by_locale(locale)
  end

  def create_translations(langs)
    locale = 0

    langs.each do |l|
      self.menu_translations.create(:name=>l, :locale=>locale)
      locale = locale + 1
    end
  end

end
