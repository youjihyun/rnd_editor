class MenuVideo < ActiveRecord::Base
	belongs_to :menu

	has_attached_file :video,
		:styles => {
			:medium => { :geometry => '640x480', :format =>'mp4'},
			:small => { :geometry => '320x240', :format =>'mp4'}
		},
		:processors => [:ffmpeg, :qtfaststart]

  validates_attachment_content_type :video, :content_type => /\Avideo\/.*\Z/

  def url(type)
  	self.video.url(type)
  end
end