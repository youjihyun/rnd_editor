class Restaurant < ActiveRecord::Base
	has_many :menus, dependent: :destroy
	has_many :categories, dependent: :destroy
	has_many :restaurant_photos, dependent: :destroy
  has_many :comments
  has_many :user_sym_points

  belongs_to :user

	def photos
  	self.restaurant_photos.order('seq')
  end

  def create_photos(photos)
    seq = self.photos.count

    photos.each do |p|
      seq = seq + 1
      self.restaurant_photos.create(:photo=>p, :seq=>seq)
    end
  end

  def main_photo(type)
    if self.restaurant_photos.order('seq').first
      self.restaurant_photos.order('seq').first.url(type)
    else
      "/assets/logo/restaurant#{RestaurantPhoto::PHOTO_STYLE[type]}.png"
    end
  end

  def week_days
    ret = []

    ret << (self.sun_d ? self.open_time.to_s(:time) : "Closed")
    ret << (self.mon_d ? self.open_time.to_s(:time) : "Closed")
    ret << (self.tue_d ? self.open_time.to_s(:time) : "Closed")
    ret << (self.wed_d ? self.open_time.to_s(:time) : "Closed")
    ret << (self.thu_d ? self.open_time.to_s(:time) : "Closed")
    ret << (self.fri_d ? self.open_time.to_s(:time) : "Closed")
    ret << (self.sat_d ? self.open_time.to_s(:time) : "Closed")

    ret
  end
end
