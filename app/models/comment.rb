class Comment < ActiveRecord::Base
	belongs_to :restaurant

	def rand_photo
		menu_ids = self.restaurant.menus.map(&:id)

		selected_menu_id = menu_ids[Random.rand(menu_ids.count)]

		menu = Menu.find(selected_menu_id)
		menu.main_photo(:large)
	end
end
