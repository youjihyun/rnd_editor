class CreateMenus < ActiveRecord::Migration
  def change
    create_table :menus do |t|
    	t.string :name
    	t.text :description
    	t.integer :price

    	t.float :price_usd
    	t.float :price_jpy
    	t.float :price_cny

      t.timestamps null: false
    end
  end
end
