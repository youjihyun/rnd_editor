class CreateUserSymPoints < ActiveRecord::Migration
  def change
    create_table :user_sym_points do |t|
    	t.integer :restaurant_id
    	t.integer :user_id

      t.timestamps null: false
    end
  end
end
