class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
    	t.integer :restaurant_id
    	t.string :user_email
    	t.text :description

    	t.decimal :star_point

      t.timestamps null: false
    end
  end
end
