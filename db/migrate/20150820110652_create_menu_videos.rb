class CreateMenuVideos < ActiveRecord::Migration
  def change
    create_table :menu_videos do |t|
    	t.integer :seq
    	t.integer :menu_id

      t.timestamps null: false
    end
  end
end
