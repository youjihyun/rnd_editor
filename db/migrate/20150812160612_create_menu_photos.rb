class CreateMenuPhotos < ActiveRecord::Migration
  def change
    create_table :menu_photos do |t|
    	t.integer :seq
    	t.integer :menu_id

      t.timestamps null: false
    end
  end
end
