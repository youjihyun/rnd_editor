class AddPhotoToRestaurantPhotos < ActiveRecord::Migration
  def change
  	add_attachment :restaurant_photos, :photo
  end
end
