class CreateRestaurants < ActiveRecord::Migration
  def change
    create_table :restaurants do |t|
    	t.string :user_email
    	t.string :name
    	t.text :description
    	t.string :r_code
    	t.string :r_pwd

      t.integer :user_id

      # 오픈 / 클로즈 시간
      t.time :open_time
      t.time :close_time

      # 오픈 요일 
      t.boolean :mon_d
      t.boolean :tue_d
      t.boolean :wed_d
      t.boolean :thu_d
      t.boolean :fri_d
      t.boolean :sat_d
      t.boolean :sun_d

      # 위동 / 경도 
      t.float :x_pos
      t.float :y_pos

      # 주소
      t.integer :postcode
      t.string :address

      # 전화번호
      t.string :phone

      t.timestamps null: false
    end
  end
end
