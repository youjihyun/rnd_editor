class AddCategoryIdToMenus < ActiveRecord::Migration
  def change
    add_column :menus, :category_id, :integer
    add_index :menus, :category_id
  end
end
