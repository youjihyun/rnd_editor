class CreateMenuTranslations < ActiveRecord::Migration
  def change
    create_table :menu_translations do |t|
    	t.integer :menu_id
    	t.integer :locale
    	t.string :name
    	t.text :description

      t.timestamps null: false
    end
  end
end
