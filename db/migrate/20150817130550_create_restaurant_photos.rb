class CreateRestaurantPhotos < ActiveRecord::Migration
  def change
    create_table :restaurant_photos do |t|
    	t.integer :seq
    	t.integer :restaurant_id

      t.timestamps null: false
    end
  end
end
