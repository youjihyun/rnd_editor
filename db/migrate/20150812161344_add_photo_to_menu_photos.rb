class AddPhotoToMenuPhotos < ActiveRecord::Migration
  def change
  	add_attachment :menu_photos, :photo
  end
end
